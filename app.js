const express = require('express');
const logger = require('morgan');
const fileUpload = require('express-fileupload')
var cors = require('cors')
require('dotenv').config()

const indexRouter = require('./routes');

const app = express();
app.use(cors())
// Serve only the static files form the dist directory
app.use(express.static('./dist/testingAppM'));

require('./config/dbConfig');


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(fileUpload({
  limits: { fileSize: 50 * 1024 * 1024 },
}));
app.use(express.static(__dirname + '/public'));

app.use('/api/v1', indexRouter);


app.use('*', (req, res) => {
  res.send('Something Went Wrong..');
});

module.exports = app;















































// Dont Delete these lines 

// const express = require('express');
// const logger = require('morgan');
// const fileUpload  = require('express-fileupload')
// const path = require('path')
// var cors = require('cors')
// require('dotenv').config()

// const indexRouter = require('./routes');

// const app = express();
// app.use(cors())

// require('./config/dbConfig');


// app.use(logger('dev'));
// app.use(express.json());
// app.use(express.urlencoded({ extended: true }));
// app.use(fileUpload({
//   limits: { fileSize: 50 * 1024 * 1024 },
// }));
// app.use(express.static(__dirname + '/public'));


// app.use(express.static(path.join(__dirname, 'storage')));
// // build 
// app.use(express.static(process.cwd() + "/dist/testingAppM/"));
// app.use('/api', indexRouter);

// app.get('*', (req, res) => {
//   res.sendFile(process.cwd() + "/dist/testingAppM/index.html")
// })
// app.get('/', (req, res) => {
//   res.sendFile(process.cwd() + "/dist/testingAppM/index.html")

// });


// app.use('*', (req, res) => {
//   res.send('Route Not Defined...');
// });

// module.exports = app;