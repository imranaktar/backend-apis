const bcrypt = require('bcryptjs');
var jwt = require("jsonwebtoken");

const hashPassword = (password) => new Promise(async (resolve, reject) => {
    try {
        resolve(await bcrypt.hash(password, 12))
    } catch (error) {
        reject(error)
    }
});

const comparewPassword = (clientPass, dbPass) => new Promise(async (resolve, reject) => {
    try {
        resolve(await bcrypt.compareSync(clientPass, dbPass))
    } catch (error) {
        reject(error)
    }
});

const verifyTokenNewUser = async (getToken)=>{
    try {
        let verifyToken =  await jwt.verify(getToken,"shhhhh",{ expiresIn: "5h"});
        tokenTime = verifyToken.exp < new Date()
        if(tokenTime == true){
            return ('Token Verify Successfully' , true)
        }else{
            return 'Invalid Token.!'
        }
    } catch (error) {
      console.log(error)
    }
}

module.exports = {
    hashPassword,
    comparewPassword,
    verifyTokenNewUser
};