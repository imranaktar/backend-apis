const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  email: {
    type: String,
  },
  firstName: {
    type: String,
  },
  lastName: {
    type: String,
  },
  password: {
    type: String,
    required: true,
    select: false,
  },
  token: {
    type: String,
    default: false,
    select: false,
  },
  createdTime: {
    type: Date,
    default: Date.now(),
  },
  updatedTime: {
    type: Date,
    default: null,
  },
});

const User = mongoose.model("users", userSchema);
module.exports = User;
