var express = require('express');
var router = express.Router();

const userRoutes = require('./users.routes');

router.use('/users', userRoutes);

module.exports = router;