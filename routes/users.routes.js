var express = require('express');
var router = express.Router();
/* User Controllers. */
const { userController } = require('../controller');
/* User Middleware. */
const { userMiddleware } = require('../middleware');
/* User listing. */
router.post('/addUser', userController.addUser);
router.get('/allUser', userController.allUser);
// router.get('/allUser', userMiddleware.verifyToken ,userController.allUser);
router.get('/findSingleUser/:id', userController.findSingleUser);
router.put('/editSingleUser/:id', userController.editSingleUser);
router.delete('/deleteSingleUser/:id', userController.deleteSingleUser);
router.get('/verify/:id', userController.verifyAccount);
router.post('/login', userController.loginUser);

module.exports = router;
