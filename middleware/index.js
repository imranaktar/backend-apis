const userMiddleware = require('./tokenVerify.middleware');
const VerifyRoleMiddleware = require('./verifyRole.middleware');


module.exports = {
    userMiddleware,
    VerifyRoleMiddleware
}