const { User } = require("../models");
var jwt = require("jsonwebtoken");
const { setResponse } = require("../helpers");
const verifyToken = async (req, res, next) => {
  try {
    res.header("Access-Control-Allow-Origin", "*");
    res.setHeader(
      "Access-Control-Allow-Methods",
      "GET, POST, OPTIONS, PUT, PATCH, DELETE"
    );
    res.header("Access-Control-Allow-Headers", "Authorization");
    // console.log(req.headers['authorization']);
    let headerToken = req.headers["authorization"];
    console.log(headerToken, "header Token ");
    let splitToken = headerToken.split(" ");
    console.log(splitToken, "header Token split");
    let userId = splitToken[0];
    console.log(userId, "header Token userId");
    let userToken = splitToken[1];
    console.log(userToken, "header Token userToken");
    if (!userToken) {
      res.send({
        type: "invalidToken",
        message: `Invalid Token.! Login Again`,
      });
    }
    let data = await User.findOne({ _id: userId });
    if (data) {
      var decoded = jwt.verify(userToken, "shhhhh");
      if (decoded) {
        console.log(decoded, "decoded");
        tokenTime = decoded.exp < new Date();
        if (tokenTime == true) {
          console.log("Token Verify Successfully");
        } else {
          console.log("Invalid Token.! Login Again");
          res.send({
            type: "invalidToken",
            message: `Invalid Token.! Login Again`,
          });
        }
      } else {
        res.send({
          type: "invalidToken",
          message: `Invalid Token.! Login Again`,
        });
      }
    } else {
      res.send({
        type: "invalidToken",
        message: `User Not Found.! Login Again`,
      });
    }
    next();
  } catch (error) {
    // console.log(error)
  }
};
module.exports = { verifyToken };