const { userService } = require('../services');
const { setResponse } = require('../helpers');




const addUser = async (req, res) => {
    try {
        const data = await userService.signUp(req)
        setResponse(res, data)
    } catch (error) {
        setResponse(res, { type: 'serverError' })
    }
};
const allUser = async (req, res) => {
    try {
        const data = await userService.findAllUser(req);
        setResponse(res, data)
    } catch (error) {
        setResponse(res, { type: 'serverError' })
    }
}
const findSingleUser = async (req, res) => {
    try {
        const data = await userService.findSingleUser(req);
        setResponse(res, data)
    } catch (error) {
        setResponse(res, { type: 'serverError' })
    }
}
const editSingleUser = async (req, res) => {
    try {
        const data = await userService.editSingleUser(req);
        setResponse(res, data)
    } catch (error) {
        setResponse(res, { type: 'serverError' })
    }
}
const deleteSingleUser = async (req, res) => {
    try {
        const data = await userService.deleteSingleUser(req);
        setResponse(res, data)
    } catch (error) {
        setResponse(res, { type: 'serverError' })
    }
}

const loginUser = async (req, res) => {
    try {
        const data = await userService.loginUser(req);
        console.log(data, 'retuern from promise')
        res.send(data)
    } catch (error) {

    }
}

const verifyAccount = async (req, res) => {
    try {
        let data = await userService.verifyAccount(req);
        res.send(data)
    } catch (error) {
        console.log(error)
    }
}

module.exports = { addUser, allUser, findSingleUser, editSingleUser, deleteSingleUser, loginUser, verifyAccount }