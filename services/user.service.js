const _ = require("lodash");
var jwt = require("jsonwebtoken");
const fs = require("fs")
const fileUpload = require('express-fileupload');
const { User } = require("../models");
const { userHelper, nodeMailerHelper } = require("../helpers");
const { update } = require("lodash");
const dir = './public/userImages'


const signUp = async (req) => {
  try {
    console.log(req.body, 'body')
    // console.log(req.files.image,'body')
    // if (!fs.existsSync(dir)) {
    //   await fs.promises.mkdir(dir, { recursive: true })
    // }
    // let userImage = await storeUserImage(req.files.image);
    const { body } = req;
    // body.image = userImage;
    const reqData = body;
    reqData.password = await userHelper.hashPassword(reqData.password);
    const result = await User.findOne({ email: body.email });
    if (result) {
      return {
        type: "existError",
        message: `Already Exist Email: ${body.email}.!  Try Diffrent`,
      };
    } else {
      let token_id = await jwt.sign({}, "shhhhh", {
        expiresIn: "1d",
      });
      body.token = token_id
      const data = await User.create(body);
      console.log(data, 'data')
      if (data) {
        // let findId = await User.findOne({ email : body.email }).populate('role');
        // getId = findId._id;
        // let sendMail = await nodeMailerHelper.nodeMailer(req,getId);
        return {
          type: "success",
          message: `${data.email} register record successfully `,
          data,
        };
      } else {
        return { type: "notFound", message: `Record Not created` };
      }
    }
  } catch (error) {
    console.log(error)
  }
};

// const signUp = async (req) => {
//   try {
//     console.log(req.body,'body')
//     console.log(req.files.image,'body')
//     if (!fs.existsSync(dir)) {
//       await fs.promises.mkdir(dir, { recursive: true })
//     }
//     let userImage = await storeUserImage(req.files.image);
//     const {body}= req;
//     body.image = userImage;
//     const reqData = body;
//     reqData.password = await userHelper.hashPassword(reqData.password);
//     const result = await User.findOne({ email: body.email }).populate('role');
//     if (result) {
//       return {
//         type: "existError",
//         message: `Already Exist Email: ${body.email}.!  Try Diffrent`,
//       };
//     } else {
//       let token_id = await jwt.sign({}, "shhhhh", {
//         expiresIn: "1d",
//       });
//       body.token = token_id
//       const data = await User.create(body);
//       if (data) {
//         let findId = await User.findOne({ email : body.email }).populate('role');
//         getId = findId._id;
//         let sendMail = await nodeMailerHelper.nodeMailer(req,getId);
//         return {
//           type: "success",
//           message: `${data.firstName} ${sendMail} Please Verify Using Gmail Link `,
//           data,
//         };
//       } else {
//         return { type: "notFound", message: `Record Not created` };
//       }
//     }
//   } catch (error) {
//     console.log(error)
//   }
// };
const findAllUser = async ({ query }) => {
  console.log("llll")
  try {
    const data = await User.find(query);
    if (data) {
      return { type: "success", message: `Record Found Successfully`, data };
    } else {
      return { type: "notFound", message: `Record Not Found` };
    }
  } catch (error) {
    throw error;
  }
};
const findSingleUser = async ({ params }) => {
  try {
    let id = params.id;
    const data = await User.findOne({ _id: id });
    if (data) {
      return { type: "success", message: `Record Found Successfully`, data };
    } else {
      return { type: "notFound", message: `Record Not Found` };
    }
  } catch (error) {
    throw error;
  }
};

const editSingleUser = async ({ body, params }) => {
  try {
    let id = params.id;
    let authUser = await User.findOne({ _id: id });
    if (authUser._id) {
      if (authUser.email == body.email) {
        delete body.email;
        const data = await User.findByIdAndUpdate(id, body, { new: true });
        return { type: "success", message: `Record Update Successfully`, data };
      } else {
        const notUniqueUser = await User.findOne({ email: body.email });
        if (notUniqueUser) {
          return {
            type: "existError",
            message: `Already Exist Email:'${body.email}'.! Try Diffrent`,
          };
        } else {
          const data = await User.findByIdAndUpdate(id, body, { new: true });
          return {
            type: "success",
            message: `Record Update Successfully`,
            data,
          };
        }
      }
    } else {
      return { type: "notFound", message: `Record Not Found` };
    }
  } catch (error) {
    throw error;
  }
};

const deleteSingleUser = async ({ params }) => {
  try {
    let id = params.id;
    let data = await User.findByIdAndDelete({ _id: id });
    if (data) {
      return { type: "success", message: `Record Delete Successfully`, data };
    } else {
      return { type: "notFound", message: `Record Not Found` };
    }
  } catch (error) {
    throw error;
  }
};

const loginUser = async ({ body }) => {
  try {
    let data = await User.findOne({ email: body.email }).select("+password");
    if (data) {
      let matchPassword = await userHelper.comparewPassword(
        body.password,
        data.password
      );
      if (matchPassword) {
        console.log("password match");
        let token = await jwt.sign({ id: data._id }, "shhhhh", {
          expiresIn: "5h",
        });
        if (token) {
          const update = await User.updateOne(
            { _id: data._id },
            { token: token },
            { new: true }
          );

          return {
            type: "success",
            message: "Login Successfully",
            data: `${data._id} ${token}`,
            role: data.role
          };
        } else {
          return {
            type: "notFound",
            message: "Token Not Genrate",
          };
        }
      } else {
        return {
          type: "notFound",
          message: "Password Not Match",
        };
      }
    } else {
      return {
        type: "notFound",
        message: "Email Not Found",
      };
    }
  } catch (error) {
    console.log("error");
  }
};

// const loginUser = async ({ body }) => {
//   try {
//     let data = await User.findOne({ email: body.email }).select("+password");
//     if (data) {
//       let matchPassword = await userHelper.comparewPassword(
//         body.password,
//         data.password
//       );
//       if (matchPassword) {
//         console.log("password match");
//         let token = await jwt.sign({ id: data._id }, "shhhhh", {
//           expiresIn: "1h",
//         });
//         if (token) {
//           const update = await User.updateOne(
//             { _id: data._id },
//             { token: token },
//             { new: true }
//           );

//           return {
//             type: "success",
//             message: "Login Successfully",
//             data: `${data._id} ${token}`,
//           };
//         }else{
//           return {
//             type: "notFound",
//             message: "Token Not Genrate",
//           };
//         }
//       } else {
//         return {
//           type: "notFound",
//           message: "Password Not Match",
//         };
//       }
//     } else {
//       return {
//         type: "notFound",
//         message: "Email Not Found",
//       };
//     }
//   } catch (error) {
//     console.log("error");
//   }
// };

const verifyAccount = async ({ params }) => {
  try {
    let id = params.id;
    const data = await User.findOne({ _id: id }).select("+token");
    if (data) {
      let getToken = data.token;
      let verifyToken = await userHelper.verifyTokenNewUser(getToken);
      if (verifyToken == true) {
        const verifyUserAccount = await User.findByIdAndUpdate(id, { isVerified: true }, { new: true });
        let data = verifyUserAccount
        return {
          type: "success",
          message: "User Register Successfully",
          data: data
        };
      } else {
        return {
          type: "notFound",
          message: "Invalid Token.! Try Again",
        };
      }
    } else {
      return {
        type: "notFound",
        message: "Record Not Found",
      };
    }
  } catch (error) {
    console.log(error)
  }
}

const storeUserImage = (file) => new Promise((resolve, reject) => {
  const fileName = `${new Date().valueOf()}-${file.name}`;
  file.mv(`./public/userImages/${fileName}`, (err) => {
    if (err) {
      reject(err);
    }
    resolve(`http://localhost:11000/userImages/${fileName}`);
  });
})

module.exports = {
  signUp,
  findAllUser,
  findSingleUser,
  editSingleUser,
  deleteSingleUser,
  loginUser,
  verifyAccount
};
